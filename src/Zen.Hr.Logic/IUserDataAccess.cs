using System.Collections.Generic;
namespace Zen.Hr.Logic
{
    public interface IUserDataAccess
    {
        List<User> GetAllUsers();
        List<User> GetAllActiveUsers();
    }
}