namespace Zen.Hr.Logic
{
    public class User
    {
        public bool IsActive { get; set; }
        public string Name { get; set; }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            User p = obj as User;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Name == p.Name) && (IsActive == p.IsActive);
        }

        public bool Equals(User u)
        {
            // If parameter is null return false:
            if ((object)u == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Name == u.Name) && (IsActive == u.IsActive);
        }
    }
}