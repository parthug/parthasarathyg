using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;
        Mock<IUserDataAccess> userDataAccess;

        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            List<User> actualUsers = new List<User>();
            List<User> expectedUsers = new List<User>();

            expectedUsers.Add(new User { Name = "Name1", IsActive = true });
            expectedUsers.Add(new User { Name = "Name2", IsActive = false });
            expectedUsers.Add(new User { Name = "Name3", IsActive = false });
            expectedUsers.Add(new User { Name = "Name4", IsActive = true });
            expectedUsers.Add(new User { Name = "Name5", IsActive = true });

            actualUsers = concern.GetUsers(false);
            CollectionAssert.AreEqual(expectedUsers, actualUsers);
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            List<User> actualUsers = new List<User>();
            List<User> expectedUsers = new List<User>();

            expectedUsers.Add(new User { Name = "Name1", IsActive = true });
            expectedUsers.Add(new User { Name = "Name4", IsActive = true });
            expectedUsers.Add(new User { Name = "Name5", IsActive = true });

            actualUsers = concern.GetUsers(true);
            CollectionAssert.AreEqual(expectedUsers, actualUsers);
        }

        [TestInitialize()]
        public void Initialize()
        {
            userDataAccess = new Mock<IUserDataAccess>();

            userDataAccess.Setup(m => m.GetAllUsers())
                .Returns(
                new List<User> {
            new User{ 
                Name = "Name1", 
                IsActive = true
            }
            , new User{
                Name = "Name2", 
                IsActive = false
            }
            , new User{
                Name = "Name3", 
                IsActive = false
            }
            , new User{
                Name = "Name4",
                IsActive = true   
            }
            , new User{
                Name = "Name5", 
                IsActive = true   
            }
        });

            userDataAccess.Setup(m => m.GetAllActiveUsers())
                .Returns(new List<User> {
            new User{
                Name = "Name1", 
                IsActive = true
            }
            , new User{
                Name = "Name4", 
                IsActive = true
            }
            , new User{
                Name = "Name5", 
                IsActive = true
            }
        });

            concern = new UserService(userDataAccess.Object);
        }
    }
}